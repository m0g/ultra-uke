
export default class Api {
  search(query) {
    const q = encodeURIComponent(query);

    return fetch(`http://www.ultimate-guitar.com/search.php?search_type=title&value=${q}`)
      .then(data => data.text())
      .then(html => {
        const match = html.match(/window.UGAPP.store.page\s=\s([^;]+);/m); 

        if (match) {
          return match[1];
        }

        throw 'empty';
      })
      .then(match => JSON.parse(match))
      .then(json => json.data.results);
  }

  get(tabUrl) {
    return fetch(tabUrl)
      .then(data => data.text())
      .then(html => {
        const match = html.match(/window.UGAPP.store.page\s=\s(.+);/m); 

        if (match) {
          return match[1];
        }

        throw 'empty';
      })
      .then(match => JSON.parse(match))
      .then(json => json.data);
 
  }
}