import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, FlatList } from 'react-native';
import Api from './api';

const api = new Api();

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { results: [] };
  }

  onSubmit() {
    api.search(this.state.text)
      .then(data => {
        console.log(data);
        this.setState({ results: data });
      })
      .catch(err => {
        console.log('no results');
      })
  }

  onPress(song) {
    console.log(song);
    api.get(song.tab_url)
      .then(data => {
        console.log('data', data.tab_view.wiki_tab.content);
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.searchBar}
          onSubmitEditing={this.onSubmit.bind(this)}
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
        />
        <FlatList
          style={styles.results}
          data={this.state.results}
          renderItem={({item, index}) =>
            <Button 
              title={item.song_name}
              key={index} 
              onPress={() => this.onPress(item)}></Button>
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchBar: {
    width: '100%',
    marginTop: 16,
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    flexGrow: 1
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  results: {
    flexGrow: 8
  }
});
